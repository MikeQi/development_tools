#!/bin/bash

#Install nvidia-docker and nvidia-docker-plugin
wget -P /tmp https://github.com/NVIDIA/nvidia-docker/releases/download/v1.0.1/nvidia-docker_1.0.1-1_amd64.deb
dpkg -i /tmp/nvidia-docker*.deb && rm /tmp/nvidia-docker*.deb

# Test nvidia-smi
nvidia-docker run --rm nvidia/cuda nvidia-smi

# NOTE:WE NEED TO FIGURE OUT HOW TO RUN THINGS OUTSIDE BEING "SU"
usermod -aG docker ${USER}
usermod -aG nvidia-docker ${USER}